App.Models.Empleado = Backbone.Model.extend({
	url 	: 	url_empleados,
	defaults : {
		nombre		: "",
		apellido	: ""
	},
	initialize : function(){
		this.on('invalid', function(model){
			Materialize.toast(model.validationError, 4000);
		});
	},
	validate : function(model, options){
		if(this.get('nombre') === ''){
			return "El nombre no puede estar vacío";
		}
		if(this.get('apellido') === ''){
			return "El appellido no puede estar vacío";
		}
		if(window.app.state == 'agregar'){
			var model = window.collections.empleados.findWhere({
				nombre		: this.get('nombre'),
				apellido 	: this.get('apellido')
			});
			if(typeof model === 'object'){
				return "Ya has registrado a ese empleado";
			}
		}
	},
	rollback : function(){
		this.set(this.previousAttributes());
	}
});