<?php

class Resp{
	
	public static function success($status, $message){
		return Response::json([
			'status' 	=> 	$status,
			'message' 	=>	$message
		]);
	}

}