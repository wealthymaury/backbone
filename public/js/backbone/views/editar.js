App.Views.Editar = App.Views.Base.extend({
	el : '#edit-view',
	events : {
		'click .volver' : 'volver',
		'click .save'	: 'save'
	},
	initialize 	: 	function(){
		console.log('vista editar inicializada');
		var self = this;
		window.routers.controller.on('route', function(route){
			switch(route){
				case 'editar':
					self.$el.removeClass('hide');
					self.$el.fadeIn('slow');
					break;
				default:
					self.$el.hide();
					break;
			}
		});
	},
	getDataEmpleado : function(){
		return {
			nombre: this.$('#first_name_edit').val(),
			apellido: this.$('#last_name_edit').val()
		};	
	},
	setModelToEdit : function(model){
		this.model = model;
		this.setContentIntoInputs();
	},
	setContentIntoInputs : function(){
		this.setContentToInput('first_name_edit', this.model.get('nombre'));
		this.setContentToInput('last_name_edit', this.model.get('apellido'));
	},
	cleanInputs : function(){
		this.setContentToInput('first_name_edit', '');
		this.setContentToInput('last_name_edit', '');
	},
	save : function(e){
		var self = this;
		e.preventDefault();
		this.model.set(this.getDataEmpleado());
		this.model.save(null,{
			success : function(model, resp){
				self.toast(resp);
			},
			error : function(model, resp){
				self.toast(resp);
				model.rollback();
			}
		});
	},
	volver : function(){
		this.cleanInputs();
		Backbone.history.navigate('', {trigger:true});
	}
});