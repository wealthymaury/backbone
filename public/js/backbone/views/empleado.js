App.Views.Empleado = App.Views.Base.extend({
	tagName : 'li',
	className : 'collection-item avatar fix-padding-left',
	events : {
		'click .editar' 	: 'editar',
		'click .eliminar'	: 'eliminar',
		'click .ver'		: 'ver'
	},
	initialize 	: 	function(model){
		console.log('vista empleado inicializada');
		var self = this;
		this.model = model;
		this.template = _.template($('#empleado').html());

		this.model.on('destroy', function(){
			self.remove();
		});

		this.model.on('change', function(model){
			self.render();
		});
	},
	render : function(){
		this.$el.html(this.template(this.model.toJSON()));
		this.$el.appendTo('#empleados');
	},
	editar : function(e){
		e.preventDefault();
		window.views.editar.setModelToEdit(this.model);
		Backbone.history.navigate('editar/' + this.model.get('id'), {trigger:true});
	},
	eliminar : function(e){
		e.preventDefault();
		var self = this;
		this.model.destroy({
			success : function(model, resp){
				self.toast(resp);
				window.collections.empleados.remove(model);
			},
			error : function(model, resp){
				self.toast(resp);
			},
			wait : true
		});
	},
	ver : function(e){
		e.preventDefault();
		window.views.detalles.setModelToSee(this.model);
		Backbone.history.navigate('detalle/' + this.model.get('id'), {trigger:true});
	}
});