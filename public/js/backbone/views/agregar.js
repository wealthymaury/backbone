App.Views.Agregar = App.Views.Base.extend({
	el : '#add-view',
	events : {
		'click .volver' : 'volver',
		'click .create'	: 'create'
	},
	initialize 	: 	function(){
		console.log('vista agregar inicializada');
		var self = this;
		window.routers.controller.on('route', function(route){
			switch(route){
				case 'agregar':
					self.$el.removeClass('hide');
					self.$el.fadeIn('slow');
					break;
				default:
					self.$el.hide();
					break;
			}
		});
	},
	volver : function(){
		this.cleanFields();
		Backbone.history.navigate('', {trigger:true});
	},
	getDataEmpleado : function(){
		return {
			nombre: this.$('#first_name').val(),
			apellido: this.$('#last_name').val()
		};	
	},
	cleanFields : function(){
		this.setContentToInput('first_name', '');
		this.setContentToInput('last_name', '');
	},
	create : function(e){
		var self = this;
		var model = new App.Models.Empleado();

		e.preventDefault();
		model.set(this.getDataEmpleado());
		model.save(null,{
			success : function(model, resp){
				self.toast(resp);
				self.cleanFields();
				//debe venir un ID de back
				window.collections.empleados.add(model);
			},
			error : function(model, resp){
				self.toast(resp);
			}
		});
	}
});