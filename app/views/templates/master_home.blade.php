<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta name="mobile-web-app-capable" content="yes">

	<title>@yield('title')</title>
	@include('templates.includes.styles')
	@include('templates.includes.libreries')
</head>
<body class="grey lighten-4">

	<div class="wrapper">
		<div class="teal" style="width:100%; height:15em;">
			
		</div>

		<div class="container" style="margin-top:-10em;">
			@yield('content')
		</div>
	</div>

	
</body>
</html>