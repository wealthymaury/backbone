@extends('templates.master_body')
@section('title')
    {{"Empleados"}}
@stop

@section('content')
	<br>

	<div class="row" id="list-view">
		<div class="col s12">
			<ul class="collection with-header z-depth-1">
		        <li class="collection-header">
		        	<!--a href="#!" class="secondary-content">
		        		<i class="mdi-content-add-circle small grey-text"></i>
		        	</a-->
		        	<h4>Empleados</h4>
		        </li>
		        <div id="empleados">
			        <!--li class="collection-item avatar" style="padding-left: 72px; !important">
				      	<i class="mdi-action-account-circle circle teal"></i>
				      	<span class="title">Title</span>
				      	<p>First Line <br>
				         	Second Line
				      	</p>
				      	<a href="#!" class="secondary-content">
			    			<i class="mdi-editor-mode-edit small grey-text"></i>
			    			<i class="mdi-action-delete small grey-text"></i>
			    			<i class="mdi-content-send small grey-text"></i>
			    		</a>
				    </li-->
		        </div>
		    </ul>
		</div>
	</div>

	<div class="row hide" id="add-view">
  		<form class="col s12">
  			<div class="card-panel">
  				<h4>
  					<i class="mdi-navigation-arrow-back small icon-mini volver"></i>
  					Registro de empleado
  				</h4>
  				<br>
		      	<div class="row">
			        <div class="input-field col s12 m6">
			          	<input id="first_name" type="text" class="validate">
			          	<label for="first_name">Nombre</label>
			        </div>
			        <div class="input-field col s12 m6">
			          	<input id="last_name" type="text" class="validate">
			          	<label for="last_name">Apellido</label>
			        </div>
			        <div class="input-field col s12 right-align">
					    <a class="waves-effect waves-teal btn-flat volver">CANCELAR</a>
					    <a class="waves-effect waves-teal btn-flat create">ACEPTAR</a>		
			        </div>
	  			</div>
	  		</div>
	    </form>
  	</div>

  	<div class="row hide" id="edit-view">
  		<form class="col s12">
  			<div class="card-panel">
  				<h4>
  					<i class="mdi-navigation-arrow-back small icon-mini volver"></i>
  					Editar empleado
  				</h4>
  				<br>
		      	<div class="row">
			        <div class="input-field col s12 m6">
			          	<input id="first_name_edit" type="text" class="validate">
			          	<label for="first_name_edit">Nombre</label>
			        </div>
			        <div class="input-field col s12 m6">
			          	<input id="last_name_edit" type="text" class="validate">
			          	<label for="last_name_edit">Apellido</label>
			        </div>
			        <div class="input-field col s12 right-align">
					    <a class="waves-effect waves-teal btn-flat volver">CANCELAR</a>
					    <a class="waves-effect waves-teal btn-flat save">ACEPTAR</a>		
			        </div>
	  			</div>
	  		</div>
	    </form>
  	</div>

	<div class="row hide" id="details-view">
		<div class="col s12">
			<ul class="collection with-header z-depth-1">
		        <li class="collection-header">
		        	<!--a href="#!" class="secondary-content">
		        		<i class="mdi-content-add-circle small grey-text"></i>
		        	</a-->
		        	<h4>
		        		<i class="mdi-navigation-arrow-back small icon-mini volver"></i>
		        		Detalle de empleado
		        	</h4>
		        </li>
		        <li class="collection-item avatar" style="padding-left: 72px; !important">
			      	<i class="mdi-action-account-circle circle teal"></i>
			      	<span class="title" id="nombre"></span>
			      	<p>
			      		<span id="apellido"></span><br>
			         	<!--span id="apellido"></span-->
			      	</p>
			      	<a href="#!" class="secondary-content">
		    			<i class="mdi-editor-mode-edit grey-text icon-mini editar"></i>
		    			<i class="mdi-action-delete grey-text icon-mini eliminar"></i>
		    		</a>
			    </li>
			    <li>
			    	<h5 style="margin-left:1em;">Telefonos</h5>
			    	<ul class="collection" id="telefonos">
				        <!--li class="collection-item dismissable">
				        	<div>
				        		Telefono
				        		<a href="#!" class="secondary-content">
				        		<i class="mdi-content-send"></i></a>
				        	</div>
				        </li-->
			      </ul>
			    </li>
		    </ul>
		</div>
	</div>

    <div class="fixed-action-btn btn-floating-helper" style="bottom: 45px; right: 24px;">
	    <a class="btn-floating btn-large teal">
	      	<i class="large mdi-navigation-apps"></i>
	    </a>
	    <ul>
	      	<li>
	      		<a class="btn-floating red list">
	      			<i class="large mdi-action-description"></i>
	      		</a>
	      	</li>
	      	<li>
	      		<a class="btn-floating green darken-1 add">
	      			<i class="large mdi-content-add"></i>
	      		</a>
	      	</li>
	    </ul>
	</div>

@stop