App.Views.FormLogin = App.Views.Base.extend({
	el : 'form',
	events : {
		'click #btn-login' : 'login',
	},
	initialize 	: 	function(){
		console.log('vista formulario inicializada');
		this.model = new App.Models.Credential();
	},
	getCredential : function(){
		return {
			username : this.$('#username').val(),
			password : this.$('#password').val()
		};
	},
	login : function(e){
		var self = this;
		e.preventDefault();
		this.model.set(this.getCredential());
		this.model.save(null, {
			success : function(model, resp){
				self.toast(resp);
				window.location = "empleados";
			},
			error : function(model, resp){
				self.toast(resp);
				model.rollback();
			}
		});
	}
});