App.Views.Detalles = App.Views.Base.extend({
	el : '#details-view',
	events : {
		'click .volver' 	: 'volver',
		'click .editar' 	: 'editar',
		'click .eliminar'	: 'eliminar',
	},
	initialize 	: 	function(){
		console.log('vista detalles inicializada');
		var self = this;
		window.routers.controller.on('route', function(route){
			switch(route){
				case 'detalle':
					self.$el.removeClass('hide');
					self.$el.fadeIn('slow');
					break;
				default:
					self.$el.hide();
					break;
			}
		});
	},
	setModelToSee : function(model){
		this.model = model;
		this.$('#nombre').html(this.model.get('nombre'));
		this.$('#apellido').html(this.model.get('apellido'));
		window.collections.telefonos.reset();
		window.collections.telefonos.add(this.model.get('telefonos'));
	},
	clean : function(){
		this.$('#nombre').html();
		this.$('#apellido').html();
	},
	volver : function(){
		this.clean();
		Backbone.history.navigate('', {trigger:true});
	},
	editar : function(e){
		e.preventDefault();
		window.views.editar.setModelToEdit(this.model);
		Backbone.history.navigate('editar/' + this.model.get('id'), {trigger:true});
	},
	eliminar : function(e){
		e.preventDefault();
		var self = this;
		this.model.destroy({
			success : function(model, resp){
				self.toast(resp);
				self.volver();
				window.collections.empleados.remove(model);
			},
			error : function(model, resp){
				self.toast(resp);
			},
			wait : true
		});
	}
});