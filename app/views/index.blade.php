@extends('templates.master_home')
@section('title')
    {{"Bienvenido"}}
@stop

@section('content')
	<div class="card-panel">
		<h1>Login</h1>
		<h4>Escribe tus datos</h4>
		<div class="row">
		    <form class="col s12">
		      	<div class="row">
		        	<div class="input-field col s12 m6">
		          		<input id="username" type="text" class="validate" autocomplete="off">
		          		<label for="username">Usuario</label>
		        	</div>
		        	<div class="input-field col s12 m6">
		          		<input id="password" type="text" class="validate" autocomplete="off">
		          		<label for="password">Contraseña</label>
		        	</div>
		      	</div>
		      	<div class="row">
		      		<div class="col s12">
		      			<a id="btn-login" class="waves-effect waves-light btn">Login</a>
		      		</div>
		      	</div>
		    </form>
	  	</div>
	</div>

  	@include('init_index')
@stop