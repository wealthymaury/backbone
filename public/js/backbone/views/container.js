App.Views.Container = App.Views.Base.extend({
	el : 'body',
	events : {
		'click .list' 	: 'navigateToList',
		'click .add'	: 'navigateToAdd',
		'click .refresh': 'refresh'
	},
	initialize 	: 	function(){
		console.log('vista contenedor inicializada');
	},
	setLoadingState : function(){
		this.$('.loader').fadeIn();
	},
	setNormalState : function(){
		this.$('.loader').hide();
		this.$('.wrapper').animsition({
		    inClass               :   'zoom-in-lg',
		    inDuration            :    1500,
		    loading               :    false
		}).one('animsition.end',function(e){
			$('.btn-floating-helper').fadeIn();
		});
	},
	navigateToList : function(e){
		e.preventDefault();
		Backbone.history.navigate('', {trigger:true});
	},
	navigateToAdd : function(e){
		e.preventDefault();
		Backbone.history.navigate('agregar', {trigger:true});
	},
	refresh : function(){
		window.location.reload();
	}
});