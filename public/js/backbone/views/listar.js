App.Views.Listar = App.Views.Base.extend({
	el : '#list-view',
	events : {
		'click .back' : 'volver'
	},
	initialize 	: 	function(){
		console.log('vista listar inicializada');
		var self = this;
		window.routers.controller.on('route', function(route){
			switch(route){
				case 'listar':
					self.$el.removeClass('hide');
					self.$el.fadeIn('slow');
					break;
				default:
					self.$el.hide();
					break;
			}
		});
	}
});