<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::table('users', function($table){
           $table->create();

           $table->increments('id');
           $table->string('username');
           $table->string('password');
           $table->rememberToken();
           $table->timestamps();
      	});
	}

	public function down()
	{
		Schema::drop('users');
	}

}
