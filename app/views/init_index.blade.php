<script type="text/javascript">
	var url_login = "{{ URL::to('login') }}";
</script>

{{ HTML::script('js/backbone/init.js'); }}
{{ HTML::script('js/backbone/models/credential.js'); }}
{{ HTML::script('js/backbone/views/base.js'); }}
{{ HTML::script('js/backbone/views/form_login.js'); }}
{{ HTML::script('js/backbone/main.js'); }}