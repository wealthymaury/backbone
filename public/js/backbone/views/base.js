App.Views.Base = Backbone.View.extend({
	toast : function(resp){
		switch(resp.status){
			case 500:
				console.error('Server: ' + resp.responseJSON.error.message);
				break;
			case 501:
				Materialize.toast(resp.responseJSON.error.message, 4000);
				break
			case 200:
				Materialize.toast(resp.message, 4000);
				break;
			default:
				console.log(resp.responseJSON.error.message);
				break
		}
	},
	setContentToInput : function(input, content){
	    var selector = '#'+input;
	    $(selector).val(content);
	    
	    if(content !== '' && content !== null){
	        $(selector).siblings('label').addClass('active');
	    }else{
	        $(selector).siblings('label').removeClass('active');
	    }
	}
});