<?php

class EmpleadosController extends BaseController 
{
	public function __construct()
	{
		$this->beforeFilter('auth');
	}

	public function getIndex()
	{
		return View::make('empleados.index');
	}

	public function getEmpleado()
	{
		return array();
	}

	public function postEmpleado()
	{
		return Resp::success(200, "Empleado creado correctamente");
	}

	public function putEmpleado()
	{
		App::abort(501, 'Error al intentar editar el empleado.');
		return Resp::success(200, "Empleado editado correctamente");
	}

	public function deleteEmpleado()
	{
		App::abort(501, 'Error al intentar eliminar el empleado.');
		return Resp::success(200, "Empleado eliminado correctamente");
	}

}
