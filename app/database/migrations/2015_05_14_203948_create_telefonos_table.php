<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTelefonosTable extends Migration {

	public function up()
	{
		Schema::table('telefonos', function($table){
           	$table->create();

           	$table->increments('id')->unsigned();
           	$table->string('telefono')->unique();
           	$table->integer('empleado_id')->unsigned();
			$table->foreign('empleado_id')->references('id')->on('empleados');
           
      	});
	}

	public function down()
	{
		Schema::drop('telefonos');
	}

}
