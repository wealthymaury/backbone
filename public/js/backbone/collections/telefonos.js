App.Collections.Telefonos = Backbone.Collection.extend({
	model 	:  	App.Models.Telefono,
	url 	: 	url_telefonos,
	initialize : function(){
		this.on('add', function(model){
			var view = new App.Views.Telefono(model);
			view.render();
		});
	}
});