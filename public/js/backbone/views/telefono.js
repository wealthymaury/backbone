App.Views.Telefono = App.Views.Base.extend({
	tagName : 'li',
	className: 'collection-item dismissable',
	events : {
		
	},
	initialize 	: 	function(model){
		console.log('vista telefono inicializada');
		var self = this;
		this.model = model;
		this.template = _.template($('#telefono').html());

		this.model.collection.on('reset', function(){
			self.remove();
		});
	},
	render : function(){
		this.$el.html(this.template(this.model.toJSON()));
		this.$el.appendTo('#telefonos');
	},
});