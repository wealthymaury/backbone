App.Models.Credential = Backbone.Model.extend({
	url 	: 	url_login,
	defaults: {
		username : "",
		password : ""	    
	},
	initialize : function(){
		this.on('invalid', function(model){
			Materialize.toast(model.validationError, 4000);
		});
	},
	validate : function(){
		if(this.get('username') === ""){
			return "EL campo usuario esta vacío."
		}
		if(this.get('password') === ""){
			return "EL campo password esta vacío."
		}
	},
	rollback : function(){
		this.set(this.previousAttributes());
	}
});