<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta name="mobile-web-app-capable" content="yes">

	<title>@yield('title')</title>
	@include('templates.includes.styles')
	@include('templates.includes.libreries')

	<style type="text/css">
		.wrapper{
			opacity: 0;
		}
		.loader, .btn-floating-helper{
			display: none;
		}
		.fix-padding-left{
			padding-left: 72px !important;
		}
		.icon-mini{
			border-radius: 50%;
			cursor: pointer;
			font-size:1.4em;
			margin:0.2em;
			padding: 0.3em;
		}
		.icon-mini:hover{
			background: #f5f5f5;
		}
	</style>
</head>
<body class="grey lighten-4">

	<div class="wrapper">
		<nav class="teal" style="padding-bottom: 15em;">
		    <div class="nav-wrapper">
		      	<a href="#!" style="font-size:2em; margin-left:1em;">BACKDIARY</a>
		      	<ul class="right">
			        <li class="add">
			        	<a href="#!"><i class="mdi-content-add"></i></a>
			        </li>
			        <li class="list">
			        	<a href="#!"><i class="mdi-action-subject"></i></a>
			        </li>
			        <li class="refresh">
			        	<a href="#!"><i class="mdi-navigation-refresh"></i></a>
			        </li>
			        <li>
			        	<a href="#!" class="dropdown-button" data-activates="dropdown1">
			        		<i class="mdi-navigation-more-vert" ></i>
			        	</a>
			        </li>
		        	<ul id='dropdown1' class='dropdown-content' style="width:150px !important; top:1em !important; right:1em !important; left:inherit !important;">
					    <li><a href="logout">Cerrar sesión</a></li>
					</ul>
		      	</ul>
		    </div>
	  	</nav>
		
		<div class="container" style="margin-top:-10em;">
			@yield('content')
		</div>
	</div>

	<div class="loader center-align">
	  	<div class="preloader-wrapper big active">
		    <div class="spinner-layer spinner-green-only">
		      	<div class="circle-clipper left">
		        	<div class="circle"></div>
		      	</div>
		      	<div class="gap-patch">
		        	<div class="circle"></div>
		      	</div>
		      	<div class="circle-clipper right">
		        	<div class="circle"></div>
		      	</div>
		    </div>
		</div>
	</div>


	@include('templates.includes.api')
</body>
</html>