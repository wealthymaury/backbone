$(document).on('ready', function(){
	console.log('Main loaded');

	window.views.formLogin = new App.Views.FormLogin();

	$('.wrapper').animsition({
	    inClass               :   'fade-in-up-lg',
	    inDuration            :    1000,
	    loading               :    false
	});
});