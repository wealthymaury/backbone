<?php

class HomeController extends BaseController {

	public function __construct(){
		$this->beforeFilter(
			'ajax', 
			array(
				'on' => 'post',
				'on' => 'put',
				'on' => 'delete'
			)
		);
	}

	public function getIndex()
	{
		return View::make('index');
	}

	public function postLogin(){

		$validacion = Validator::make(
			Input::all(),
			array(
				'username' 	=>  'required',
				'password'	=>	'required'
			)
		);

		if($validacion->fails()){
			App::abort(501, $validacion->messages()->first());
		}

		$userdata = array(
			'username' => Input::get('username'),
			'password' => Input::get('password')
		);

		if(Auth::attempt($userdata)){
			return Resp::success(200, "Login exitoso");
		}else{
			App::abort(501, 'Usuario y/o contraseña incorrectos.');
		}
	}

	public function getLogout(){
		Auth::logout();
		return Redirect::to('/');
	}

}
