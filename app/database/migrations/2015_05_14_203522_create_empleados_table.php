<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosTable extends Migration {

	public function up()
	{
		Schema::table('empleados', function($table){
           $table->create();

           $table->increments('id')->unsigned();
           $table->string('nombre')->unique();
           $table->string('apellido');
           $table->timestamps();
      	});
	}

	public function down()
	{
		Schema::drop('empleados');
	}

}
