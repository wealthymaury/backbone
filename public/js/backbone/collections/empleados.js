App.Collections.Empleados = Backbone.Collection.extend({
	model 	:  	App.Models.Empleado,
	url 	: 	url_empleados,
	initialize : function(){
		this.on('add', function(model){
			var view = new App.Views.Empleado(model);
			view.render();
		});
		
	}
});