<script type="text/javascript">
	var url_empleados = "{{ URL::to('empleados/empleado') }}";
	var url_telefonos = "";
</script>

<!--['<li class="collection-item avatar fix-padding-left">']-->
<script type="text/template" id="empleado">
  	<i class="mdi-action-account-circle circle teal"></i>
  	<span class="title">
  		<%= nombre %>
  	</span>
  	<p>
  		<%= apellido %><br>
  	</p>
  	<a href="#!" class="secondary-content">
		<i class="mdi-editor-mode-edit grey-text editar icon-mini"></i>
		<i class="mdi-action-delete grey-text eliminar icon-mini"></i>
		<i class="mdi-content-send grey-text ver icon-mini"></i>
	</a>
</script>

<!--['<li class="collection-item dismissable">']-->
<script type="text/template" id="telefono">
	<div>
		<%= telefono %>
		<a href="#!" class="secondary-content">
			<i class="mdi-content-send"></i>
		</a>
	</div>
</script>

{{ HTML::script('js/backbone/init.js'); }}

{{ HTML::script('js/backbone/models/empleado.js'); }}
{{ HTML::script('js/backbone/models/telefono.js'); }}

{{ HTML::script('js/backbone/collections/empleados.js'); }}
{{ HTML::script('js/backbone/collections/telefonos.js'); }}

{{ HTML::script('js/backbone/routers/controller.js'); }}

{{ HTML::script('js/backbone/views/base.js'); }}
{{ HTML::script('js/backbone/views/container.js'); }}
{{ HTML::script('js/backbone/views/agregar.js'); }}
{{ HTML::script('js/backbone/views/listar.js'); }}
{{ HTML::script('js/backbone/views/detalles.js'); }}
{{ HTML::script('js/backbone/views/editar.js'); }}
{{ HTML::script('js/backbone/views/empleado.js'); }}
{{ HTML::script('js/backbone/views/telefono.js'); }}

{{ HTML::script('js/backbone/app.js'); }}