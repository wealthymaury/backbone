App.Routers.Controller = Backbone.Router.extend({
	routes : {
		'' 					: 'listar',
		'agregar' 			: 'agregar',
		'detalle/:empleado' : 'detalle',
		'editar/:empleado' 	: 'editar',
	},
	listar : function(){
		console.log("Estamos en el root");
		window.app.state 	= 'root';
		window.app.empleado = null;
	},
	agregar : function(){
		console.log("Estamos en agregar");
		window.app.state 	= 'agregar';
		window.app.empleado = null;
	},
	detalle : function(empleado){
		console.log("Estamos en detalle");
		window.app.state 	= 'detalle';
		window.app.empleado = empleado;
	},
	editar : function(empleado){
		console.log("Estamos en editar");
		window.app.state 	= 'editar';
		window.app.empleado = empleado;
	}
});
