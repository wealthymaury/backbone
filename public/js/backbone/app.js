$(document).on('ready', function(){
	console.log('Main loaded');

	/*
 	 * Inicializacion del router controlador
	 */
	window.routers.controller = new App.Routers.Controller();

	/*
	 * Inicializanco vistas
	 */
	window.views.container = new App.Views.Container();
	window.views.agregar = new App.Views.Agregar();
	window.views.editar = new App.Views.Editar();
	window.views.detalles = new App.Views.Detalles();
	window.views.listar = new App.Views.Listar();
	window.views.container.setLoadingState();

	/*
	 * Inicializacion de las colecciones
	 */
	window.collections.telefonos = new App.Collections.Telefonos();
	window.collections.empleados = new App.Collections.Empleados();
	var xhr = window.collections.empleados.fetch();
	xhr.done(function(response){
		Backbone.history.start();
		Backbone.history.navigate('', {trigger:true});
		//Modifique el source de animsition para poder hacer esto :D
		_.delay(window.views.container.setNormalState, 1000);
	});

	/*
	 * Pruebas
	 */
	window.collections.empleados.add({
		id 			: '1',
		nombre		: 'maury',
		apellido 	: 'manjarez',
		telefonos 	: [
			{telefono:'123'},
			{telefono:'1234'}
		]
	});

});